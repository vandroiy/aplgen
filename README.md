# VanAPL

This project is in the very very very early phases of development. 

The purpose of this project is to use machine learning (reinforcement learning) to find the most optimal rotation for World of Warcraft classes.

### TODO-list
- [ ] Simulator
  - [x] Core
  - [] Buffs and debuffs (1 test buff implemented)
  - [x] Abilities
  - [x] Static actors
  - [ ] Replacing these with Simulationcraft
  
- [ ] ML
  - [ ] Simulation encoding (state) -- using ProtocolBuffers
  - [ ] RL agent
  - [ ] Reward function (long term rewards > short term rewards)
  - [ ] Decision tree and APL output
  
- [ ] Episodes visualization

### Why?
To get an edge in raids.
Future plans also include boss scripts and raid import; let the agents find the most optimal cooldown usage patterns for the raid (eg healing cooldowns).

### Contact me
Altho I'm doing this for fun, help and input is always appreciated. Feel free to contact me on Discord (Vanidium#2879)

### Resources for myself
- Adaptive Building of Decision Trees by Reinforcement Learning (https://pdfs.semanticscholar.org/a925/bcfc5aa2e9bd9328b973a36fd4e21d21f793.pdf)
- The First Level of Super Mario Bros. is Easy with Lexicographic Orderings and Time Travel (http://www.cs.cmu.edu/~tom7/mario/mario.pdf)
- Reinforcement Learning - Goal definition and examples (http://www.cs.upc.edu/~mmartin/Ag3-Goal%20definition%20and%20examples-4x.pdf)
- Delayed-Reinforcement Learning (http://heim.ifi.uio.no/~mes/inf1400/COOL/REF/Standford/ch11.pdf)
