#include "simulator.h"

void simulator::start_simulation(const DURATION_T& step_time, const DURATION_T& pause_interval, const int& step_debug_interval)
{
	// set the 
	auto start_time = SIMULATION_CLOCK_T::now();
	auto iteration_count = 0L;

	DURATION_T rl_time_ms;

	this->scenario = std::make_shared<simulation_scenario>();

	execution_thread = std::thread([&] {
		DURATION_T sim_time_ms;
		DURATION_T rl_time_ms;

		while (scenario->get_simulation_status() == T_SCENARIO_STATUS::running)
		{
			if (pause_interval > DURATION_T(0))
			{
				std::this_thread::sleep_for(pause_interval);
			}

			// do the time calculations first, benchmarking reasons
			rl_time_ms = std::chrono::duration_cast<std::chrono::milliseconds>(SIMULATION_CLOCK_T::now() - start_time);
			sim_time_ms = step_time * iteration_count;

			// log by log frequency
			if (iteration_count % step_debug_interval == 0)
			{
				logger::get()->simulation_it(iteration_count, sim_time_ms, rl_time_ms);
			}

			scenario->step(step_time, nullptr); 

			// increase the iteration count
			++iteration_count;
		}

		logger::get()->simulation_end(iteration_count, sim_time_ms, rl_time_ms, scenario->get_simulation_status());

	});

	logger::get()->debug("Starting simulation...", "INIT");
	execution_thread.join();
}
