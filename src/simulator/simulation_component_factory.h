#pragma once

#include "simulation_component.h"
#include "simulation_scenario.h"

class simulation_component_factory;
static simulation_component_factory* component_factory_instance;

class simulation_component_factory
{
    public:
        static simulation_component_factory* get_instance()
        {
            if (component_factory_instance == nullptr)
            {
                component_factory_instance = new simulation_component_factory;
            }

            return component_factory_instance;
        }

        simulation_component* create_agent(simulation_component* parent)
        {
            return new agent(parent);
        }
};