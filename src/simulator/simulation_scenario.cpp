#include "simulation_scenario.h"

void simulation_scenario::step(const DURATION_T& delta, COMPONENT* instigator)
{
	this->sim_agent->step(delta, this);
	remaining_timeout -= delta;
}

void simulation_scenario::trigger(COMPONENT* instigator)
{
	// do nothing for now
}

void simulation_scenario::reset()
{
}

void simulation_scenario::reward_agent()
{
}

/*
* this should be a flagset probably? can the simulation end with overlapping results?
*/
T_SCENARIO_STATUS simulation_scenario::get_simulation_status()
{
	if (dummy_agent && dummy_agent->has_internal_flag(flag_dead))
		return T_SCENARIO_STATUS::finished_dead;

	if (remaining_timeout <= DSEC(0))
		return T_SCENARIO_STATUS::finished_timeout;

	return T_SCENARIO_STATUS::running;
}