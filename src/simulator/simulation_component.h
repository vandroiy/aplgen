#pragma once

#include "../util/types.h"
enum component_flag
{
    flag_base     = 0x0000,
    flag_actor    = 0x0001,
    flag_buff     = 0x0002,
    flag_debuff   = 0x0004,
    flag_action   = 0x0008,
    flag_damaging = 0x0016,
    flag_healing  = 0x0032,
    flag_periodic = 0x0064,
    flag_buffing  = 0x0128,
    flag_stacking = 0x0256
};

enum component_lifecycle
{
    flag_alive     = 0x0000,
    flag_marked_to_kill = 0x0001,
    flag_dead      = 0x0002,
    flag_ignored   = 0x0004
};

class simulation_component
{
protected:
    // stores the parent of the node in the graph
    simulation_component* parent;

    // stores the chiledre node pointers
    std::vector<simulation_component*> children {};
    
    // stores the flags associated to this container
    flagset<component_flag> flags;

    // stores flags for this component so the scenegraph can know
    flagset<component_lifecycle> internal_flags;

protected:
    virtual void add_internal_flag(const component_lifecycle flag) { internal_flags.set(flag); }
    virtual void set_internal_flag(const component_lifecycle flag) { internal_flags.reset(); add_internal_flag(flag); }
    
public:
    simulation_component(simulation_component* _parent) : parent(_parent) { }
    simulation_component(simulation_component* _parent, flagset<component_flag> flags) : parent(_parent), flags(flags) { }
    virtual ~simulation_component() = default;

    virtual void trigger(simulation_component*) = 0;
    virtual void step(const DURATION_T&, simulation_component*) = 0;

    void add_flag(const component_flag flag) { flags.set(flag);  }
    bool has_flag(const component_flag flag) const { return flags.test(flag); }
    bool has_internal_flag(const component_lifecycle flag) const { return internal_flags.test(flag); }

    simulation_component* get_parent() 
    {
        return this->parent;
    }

    void add_child(simulation_component* child, const component_lifecycle flag = flag_alive)
    {
        this->children.push_back(child);
        child->add_internal_flag(flag);
    }

    void remove_child(simulation_component* child)
    {
        child->set_internal_flag(flag_marked_to_kill);
    }

    void remove_child(const component_flag flag)
    {
        for (auto* child : children)
        {
            if (child->has_flag(flag))
            {
                remove_child(child);
            }
        }
    }

    size_t count_children()
    {
        return this->children.size();
    }

    size_t count_children(const component_flag flag)
    {
        size_t count = 0;
        for (auto* child : children)
        {
            if (child->has_flag(flag))
                ++count;
        }
        return count;
    }

    std::vector<simulation_component*> get_children()
    {
        return this->children;
    }

    std::vector<simulation_component*> get_children(const component_flag flag)
    {
        std::vector<simulation_component*> children_with_flag;
        
        for (auto* child : children)
        {
            if (child->has_flag(flag))
                children_with_flag.push_back(child);
        }

        return children_with_flag;
    }
};
