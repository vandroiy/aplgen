#pragma once

#include <iostream>
#include <thread>
#include <atomic>

#include "../util/logger.h"
#include "../util/types.h"

#include "simulation_scenario.h"

class simulator
{
protected:
	std::thread execution_thread;
	SIMULATION_CLOCK_T clock;

	// TODO: make simulation_scenario that holds the static agent (boss) and the learning agent
	std::shared_ptr<simulation_scenario> scenario;

public:
	
	simulator() = default;
	virtual ~simulator() = default;

	/*
	@brief					Starts the simulation˚
							TODO: step_time is probably the worst fucking idea I had in a while, should be calculated from iteration duration somehow.
							If you match step_time and pause_interval, you can watch the agent doing stuff it-by-it
	@step_time				Defines how much 'simulation time' does an iteration worth. Default: 33ms. Should match GCD?
	@pause_interval			Sets how much the thread will sleep between iterations, debug purposes. Default: 0ms
	@step_debug_interval	Sets after how many iterations should the simulator log.
	@
	*/
	void start_simulation(const DURATION_T& step_time = DURATION_T(33), const DURATION_T& pause_interval = DURATION_T(0), const int& step_debug_interval = 100000);
};
