#pragma once

#include "../util/types.h"
#include "simulation_component_factory.h"

class simulation_scenario : public COMPONENT
{
private:
	COMPONENT* sim_agent = nullptr;

	// we kinda need a static agent (boss dummy)
	// because at this point the agent is killing itself
	COMPONENT* dummy_agent;

	// timeout
	DURATION_T remaining_timeout; 

public:
	simulation_scenario() : COMPONENT(nullptr), remaining_timeout(DSEC(35000))
	{
		// I wanna die
		sim_agent = simulation_component_factory::get_instance()->create_agent(this); //(new agent(this))->with_action_space();
		dummy_agent = simulation_component_factory::get_instance()->create_agent(this); //new agent(this);
	}
	
	virtual ~simulation_scenario()
	{
		delete sim_agent;
		delete dummy_agent;
	}

	/*
	Resets the simulation scenario
	*/
	void reset();

	/*
	Returns the dummy agent
	*/
	const COMPONENT* get_dummy_agent() { return dummy_agent; }

	/*
	Simulation step
	*/
	void step(const DURATION_T& delta, COMPONENT* instigator) override;

	/*
	Simulation trigger is called at the start
	*/
	void trigger(COMPONENT* instigator) override;

	/*
	Returns the status of the current scenario
	*/
	T_SCENARIO_STATUS get_simulation_status();

	/*
	Rewards the agent based
	*/
	void reward_agent();
};
