#include <iostream>

#include "simulator/simulator.h"

int main()
{
	simulator* sim = new simulator;
		
	// pass some args to the sim
	sim->start_simulation();

	delete sim;
	return 0;
}