#pragma once

#include <iostream>
#include <sstream>
#include "types.h"

class logger;
static logger* logger_instance;

class logger
{
public: 
	static logger* get()
	{
		if (logger_instance == nullptr)
		{
			logger_instance = new logger();
		}

		return logger_instance;
	}

	void log(const std::stringstream& ss)
	{
		std::cout << ss.str();
	}

	void log(const char* message)
	{
		debug(message);
	}

	void debug(const char* message, const char* tag = nullptr)
	{
		std::stringstream ss("");

		if (tag != nullptr)
		{
			ss << "[" << tag << "] ";
		}

		ss << message << std::endl;
		
		log(ss);
	}

	/**
	 * obsolete, TODO: replace this by a variadic, policy based logger
	*/
	void simulation_it(const long& sim_it_count, DURATION_T& sim_time, DURATION_T& rl_time)
	{
		std::stringstream ss("");
		
		ss << "Iterations: " << sim_it_count << ". ";
		ss << "Simulation time: " << sim_time.count() << "ms. ";
		ss << "Real life time: " << rl_time.count() << "ms. " << std::endl;

		log(ss);
	}

	/**
	 * obsolete, TODO: replace this by a variadic, policy based logger
	*/
	void simulation_end(const long& sim_it_count, DURATION_T& sim_time, DURATION_T& rl_time, T_SCENARIO_STATUS scenario_status)
	{
		std::stringstream ss("");
		ss << "Simulation finished after " << sim_it_count << " iterations. ";
		ss << "Simulation time: " << (sim_time.count()*0.001) << "s. ";
		ss << "Real life time: " << rl_time.count() << "ms. " << std::endl;
		ss << "Result: " << (scenario_status == T_SCENARIO_STATUS::finished_dead ? "finished_dead" : "finished_timeout") << std::endl;

		log(ss);
	}

	/**
	 * obsolete, TODO: replace this by a variadic, policy based logger
	*/
	void error(const char* message, const char* tag = nullptr)
	{
		std::stringstream ss("");

		if (tag != nullptr)
		{
			ss << "[!ERROR!][" << tag << "] ";
		}

		ss << message << std::endl;

		log(ss);
	}
};
