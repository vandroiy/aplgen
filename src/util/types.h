/*
I vomit here every night before I cry myself to sleep

this is practically an umbrella header gone wrong
refactor this pretty please
*/

#pragma once

// time conversion
#include <chrono>
#define SEC(x) x * 1000
#define DSEC(x) DURATION_T(SEC(x))

typedef std::chrono::high_resolution_clock			SIMULATION_CLOCK_T;
typedef std::chrono::milliseconds					DURATION_T;

enum class T_SCENARIO_STATUS
{
	running,
	finished_dead,
	finished_timeout
};


// convenience function guard
#define CALL_VA(fnc, ...) if(fnc) fnc(__VA_ARGS__)
#define CALL(fnc) if (fnc) fnc()

// convenience other
#define DELETE_VECTOR(vector) for (auto* component : vector) { delete component; }; vector.clear();

#include "flagset.h"

// simulationc component pointer
#include <vector>
#include "../simulator/simulation_component.h"
typedef simulation_component COMPONENT;

// concepts
#include "../agent/concept/gcd.h"
#include "../agent/concept/health.h"
#include "../agent/concept/stats.h"

// preinclude all the shit we need
#include "../agent/agent.h"
#include "../agent/action/action.h"
//#include "../agent/action/buff.h"