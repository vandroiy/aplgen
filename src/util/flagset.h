//
//  flagset.h
//  CombatLog-Parser
//
//  Created by Greg Toth on 1/25/16.
//  Copyright Â© 2016 Greg Toth. All rights reserved.
//
//  All credit goes to luke @ http://stackoverflow.com/questions/4226960/type-safer-bitflags-in-c
//  Imported to be a bit more safe about bitflags.

#ifndef flagset_h
#define flagset_h

#include <iostream>
#include <numeric>
#include <string>

#include <initializer_list>

template <typename enumT>
class flagset
{
public:
    typedef enumT enum_type;
    typedef decltype(enumT() | enumT()) store_type;

    // Default constructor (all 0s)
    flagset() : flagset(store_type(0))
    {
    }

    // Initializer list constructor
    flagset(const std::initializer_list<enum_type>& initList)
    {
        // This line didn't work in the initializer list like I thought it would.  It seems to dislike the use of the lambda.  Forbidden, or a compiler bug?
        flags_ = std::accumulate(initList.begin(), initList.end(), store_type(0), [](enum_type x, enum_type y) { return x | y; });
    }

    // Value constructor
    explicit flagset(store_type value) : flags_(value) { }

    // Explicit conversion operator
    operator store_type() const
    {
        return flags_;
    }

    operator std::string() const
    {
        return to_string();
    }

    bool operator[](enum_type flag) const
    {
        return test(flag);
    }

    std::string to_string() const
    {
        std::string str(size(), '0');

        for (size_t x = 0; x < size(); ++x)
        {
            str[size() - x - 1] = (flags_ & (1 << x) ? '1' : '0');
        }

        return str;
    }

    flagset& set()
    {
        flags_ = ~store_type(0);
        return *this;
    }

    flagset& set(enum_type flag, bool val = true)
    {
        flags_ = (val ? (flags_ | flag) : (flags_ & ~flag));
        return *this;
    }

    flagset& reset()
    {
        flags_ = store_type(0);
        return *this;
    }

    flagset& reset(enum_type flag)
    {
        flags_ &= ~flag;
        return *this;
    }

    flagset& flip()
    {
        flags_ = ~flags_;
        return *this;
    }

    flagset& flip(enum_type flag)
    {
        flags_ ^= flag;
        return *this;
    }

    size_t count() const
    {
        // http://www-graphics.stanford.edu/~seander/bithacks.html#CountBitsSetKernighan

        store_type bits = flags_;
        size_t total = 0;
        for (; bits != 0; ++total)
        {
            bits &= bits - 1; // clear the least significant bit in set
        }
        return total;
    }

    constexpr size_t size() const
    {
        return sizeof(enum_type) * 8;
    }

    bool test(enum_type flag) const
    {
        return (flags_ & flag) > 0;
    }

    bool any() const
    {
        return flags_ > 0;
    }

    bool none() const
    {
        return flags_ == 0;
    }

private:
    store_type flags_;
};

template <typename enumT>
flagset<enumT> operator&(const flagset<enumT>& lhs, const flagset<enumT>& rhs)
{
    return flagset<enumT>(flagset<enumT>::store_type(lhs) & flagset<enumT>::store_type(rhs));
}

template <typename enumT>
flagset<enumT> operator|(const flagset<enumT>& lhs, const flagset<enumT>& rhs)
{
    return flagset<enumT>(flagset<enumT>::store_type(lhs) | flagset<enumT>::store_type(rhs));
}

template <typename enumT>
flagset<enumT> operator^(const flagset<enumT>& lhs, const flagset<enumT>& rhs)
{
    return flagset<enumT>(flagset<enumT>::store_type(lhs) ^ flagset<enumT>::store_type(rhs));
}

template <class charT, class traits, typename enumT>
std::basic_ostream<charT, traits>& operator<<(std::basic_ostream<charT, traits>& os, const flagset<enumT>& flagset)
{
    return os << flagset.to_string();
}

#endif /* flagset_h */