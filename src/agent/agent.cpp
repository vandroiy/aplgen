#include "agent.h"

#include "action/action.h"

agent* agent::with_action_space()
{
	actions.push_back(new action(this, "mutilate", action_mutilate));
	return this;
}

void agent::step(const DURATION_T& delta, COMPONENT* instigator)
{
	// this will check for gcd if it should ignore it
	// this will also trigger gcd if it should
	actions.at(0)->trigger(this);

	//then step the gcd
	this->gcd.step(delta);
}

void agent::trigger(COMPONENT* instigator)
{
	// agent trigger should do nothing I guess
}

bool agent::is_alive()
{
	return health.get_health() > 0;
}