#pragma once

#include <map>
#include <vector>

#include "concept/health.h"
#include "concept/stats.h"
#include "concept/gcd.h"
#include "interface/action_defs.h"

class agent : public COMPONENT
{
protected:
	gcd_t gcd;
	health_t health;
	statistics_t statistics;

	std::vector<COMPONENT*> actions;

  public:
	agent(COMPONENT* parent) : COMPONENT(parent) { } 
	virtual ~agent() {
		if ( !actions.empty() ) { DELETE_VECTOR(actions);	} }

	/*
	The shit I have to deal with makes me not care anymore
	*/
	/* I probably should write a proper component system so the fucking state could be easier to build up... */
	gcd_t& get_gcd() { return gcd; }

	/*
	Steps the agent
	*/
	void step(const DURATION_T& delta, COMPONENT* instigator) override;

	/*
	Triggers the agent
	*/
	void trigger(COMPONENT* instigator) override;

	/* 
	Calculate the current dps based on the actual simulation time
	*/
	void calculate_dps(const DURATION_T& time);

	/*
	Construct the action space this actor can use
	These are mainly actions
	*/
	agent* with_action_space();

	/*
	We need a way to tell that this static agent is still alive or not
	*/
	bool is_alive();

	// ASDF
	health_t& get_health_concept() { return health; }
};