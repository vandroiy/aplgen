#pragma once

#include <functional>

#include "../../util/types.h"
#include "../../util/flagset.h"
#include "../agent.h"

struct action_fnc_object
{
    struct behaviour
    {
        bool invokes_gcd = true;
        bool ignores_gcd = false;
    };

    action_fnc_object::behaviour properties;
    std::function<void(COMPONENT*)> trigger = nullptr;
};

class action : public COMPONENT
{
private:
    std::string name;
    action_fnc_object* action_functions;
    
    DURATION_T cooldown;
    DURATION_T remaining_cooldown;

public:
    action(COMPONENT* parent) : COMPONENT(parent) { }
    action(COMPONENT* parent, const char* action_name,
            action_fnc_object* action_object) :
        COMPONENT(parent), name(action_name), action_functions(action_object) { }

    ~action() = default;

    /*  
    * triggers an action
    */
    void trigger(COMPONENT* instigator) override
    {
        /* this action is not on an actor hence its invalid? */
        /* can something else happen? */
        /*auto* owner = instigator->getas<agent>();
        if (instigator && action_functions->properties.ignores_gcd)
        {
            auto* owner = dynamic_cast<agent*>(instigator);
            if (owner && !owner->get_gcd().in_progress())
            {
                CALL_VA(action_functions->trigger, instigator);

                action_functions->trigger(instigator);
                if (action_functions->properties.invokes_gcd)
                {
                    owner->get_gcd().trigger();
                }
            }
        } */
    }

    /*  
    *  steps the action
    */
    void step(const DURATION_T& delta, COMPONENT* instigator) override
    {
    }
};
