#pragma once

#include <functional>

#include "../../util/types.h"
#include "../agent.h"

struct buff_fnc_object
{
    struct behaviour
    {
    };

    action_fnc_object::behaviour properties;

    std::function<void(COMPONENT*)> trigger = nullptr;
    std::funtion<void(COMPONENT*)>  step = nullptr;
};


class buff : public COMPONENT
{
private:

    std::string name;
    buff_fnc_object* buff_functions;

    DURATION_T remaining_duration;
    DURATION_T tick_interval;
public:
    buff(COMPONENT* parent) : COMPONENT(parent) { }
    buff(COMPONENT* parent, std::string name, buff_fnc_object* buff_functions, DURATION_T duration = SEC(1), DURATION_T interval = SEC(1))
        :COMPONENT(parent), name(name), buff_functions(std::move(buff_functions)), remaining_duration(duration), tick_interval(interval) {} 

    /*  
    * first trigger of the buff
    */
    void trigger(COMPONENT* instigator) override
    {
        // not necessary to have gcds counted here, beceuse
        // buffs and debuffs (and dots) are always gcd-independent
        CALL_VA(buff_functions->trigger, instigator);
    }

    void step(const DURATION_T& delta, COMPONENT* instigator) override
    {

    }
    
};