#pragma once

#include "../../util/types.h"

/*
GDC is a special action, since it prevents any other ability/spell cast.
Like me, I'm very special for doing this. Fuck my life.
*/

struct gcd_t
{

private:
	DURATION_T progress_time;
	bool progress;
	
public:
	gcd_t()
	{
		progress_time = DURATION_T(0);
		progress = false;
	}

	/* steps the action */
	void step(const DURATION_T& delta)
	{
		if (progress)
		{
			// increment the time it's been progressing
			progress_time += delta;
			
			// if it's done then mark it done
			if (progress_time > DURATION_T(1500))
			{
				progress = false;
			}
		}
	}
	
	/*
	returns the state of the action
	*/
	bool in_progress()
	{
		return progress;
	}

	/*
	triggers the action
	*/
	void trigger()
	{
		progress_time = DURATION_T(0);
		progress = true;
	}
};