#pragma once

#include "../../util/logger.h"
#include <sstream>

struct health_defaults_t
{
	static constexpr double D_HEALTH = 10000; 
};

struct health_t
{
protected:
	double health = health_defaults_t::D_HEALTH;

public:
	/*
	Substracts a value from the health, then returns the new health
	*/
	void damage(const double& by)
	{
		this->health -= by;

		std::stringstream ss;
		ss << "Took " << by << " damage! Health changed to " << this->health << "!";

		std::cout << ss.str().c_str() << std::endl;
	}

	void heal(const double& by)
	{
		this->health += by;
	}

	/*
	Returns the health
	*/
	const double& get_health()
	{
		return this->health;
	}
};