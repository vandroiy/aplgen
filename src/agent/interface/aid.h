#pragma once

struct aid_t
{
private:
	int _id = 0;

public: 
	aid_t(const int id) : _id(id) { };

	int get_id() { return _id;  }
};