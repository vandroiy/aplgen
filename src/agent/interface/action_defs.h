#pragma once

#include <functional>
#include "action.h"
#include "agent.h"
#include "types.h"

/*
Standard action to massively hit once and generate a combo point
*/
static auto action_mutilate = []() -> action_fnc_object* {
	auto fnc_object = new action_fnc_object;

	fnc_object->properties.invokes_gcd = true;
	fnc_object->properties.ignores_gcd = false;

	fnc_object->trigger = [](COMPONENT* component){
		/*auto agentComponent = component->getas<agent>();
		
		if (agentComponent != nullptr)
		{
			agentComponent->get_health_concept().damage(10);
		}
		*/
	};

	return fnc_object;
}();

/*
Standard action to apply a ticking debuff for a period of time depending
Depending on the combo points that were used when triggering this action
*/
static auto action_rupture = []() -> action_fnc_object* {
    auto fnc_object = new action_fnc_object;

    fnc_object->properties.invokes_gcd = true;
    fnc_object->properties.ignores_gcd = false;
    //fnc_object->properties.action_type = action_type.damaging | action_type.periodic;

    fnc_object->trigger = [](COMPONENT* component){

    };

    return fnc_object;
}();